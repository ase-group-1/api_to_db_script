import traceback

import pandas as pd
import requests
import Source.constants.config as config
import xmltodict
from bs4 import BeautifulSoup as BS
import requests

class ApiHandler():

    @staticmethod
    def api_get_api_content_json(url, header=None):
        try:
            api_response = requests.get(url, header)
            if api_response.status_code == 200 or api_response.status_code == 201:
                return api_response.json()
            else:
                return {}
        except:
            traceback.print_exc()
            return {}

    @staticmethod
    def api_get_from_xml_dict(url):
        try:
            response = requests.get(url)
            data = xmltodict.parse(response.content)
            return data
        except:
            print("Exception Occured")
            traceback.print_exc()
            return dict()

    @staticmethod
    def api_get_df_from_api(url,header=None):
        try:
            api_response = requests.get(url, header)
            if api_response.status_code == 200 or api_response.status_code == 201:
                df=pd.DataFrame(api_response.json())
                return df
            else:
                return pd.DataFrame()
        except:
            traceback.print_exc()
            return pd.DataFrame()


    @staticmethod
    def fetch_latest_footfall_csv():

        url = config.FOOT_FALL_URL
        soup = BS(requests.get(url).text)
        try:

            for ultag in soup.find_all('ul', {'class': 'resource-list'})[:1]:
                for litag in ultag.find_all('a', {'target': '_blank'})[:1]:
                    latest_csv_url = litag['href']

            return latest_csv_url
        except:
            return "Not able to fetch latest CSV URL"

    @staticmethod
    def api_post_api_content_json(url, header=None):
        try:
            api_response = requests.post(url, header)
            if (api_response.status_code == 200 or api_response.status_code == 201 )and api_response.text:
                return api_response.json()
            else:
                return {}
        except:
            traceback.print_exc()
            return {}