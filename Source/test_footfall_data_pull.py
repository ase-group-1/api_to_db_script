import unittest
from Source.core.FootfallDataPull import FootfallDataPull
from Source.utilities.ApiHandler import ApiHandler


class TestFootfallDataPull(unittest.TestCase):
    def test_foot_fall_df(self):
        foot_fall_data_pull=FootfallDataPull()
        url = ApiHandler.fetch_latest_footfall_csv()
        result, df_new = foot_fall_data_pull.foot_fall_df(url)
        if result.empty:
            assert False
        else:
            assert True

    def test_foot_fall_to_db(self):
        foot_fall_data_pull = FootfallDataPull()
        result = foot_fall_data_pull.foot_fall_to_db()
        if result:
            assert True
        else:
            assert False



if __name__ == '__main__':
    unittest.main()
