import requests
import xmltodict
import json
import pandas as pd
import mysql.connector
import pyodbc
import traceback
import Source.constants.config as config

import warnings

warnings.filterwarnings("ignore")


class DataBaseHandler():

    def sql_con_db(self):
        try:
            cnx = mysql.connector.connect(user=config.DB_USERNAME, password=config.DB_PASSWORD,
                                          host=config.DB_IP, port=config.DB_PORT,
                                          database=config.DB_CONTROL_DATABASE)
            cursor = cnx.cursor()
            return cursor, cnx
        except:
            traceback.print_exc()
            print("Cannot Connect To DB! SYSTEM FAILED!")
            return False

    def check_table_exists(self, schema, table):

        crsr, cnx = self.sql_con_db()
        qry1 = "CALL sys.table_exists(%s, %s, @exists)"
        qry2 = "select @exists"
        data = (schema, table)
        test = crsr.execute(qry1, data)
        testTable = crsr.execute(qry2)
        if testTable != '':
            return True
        else:
            return False
        cnx.commit()
        crsr.close()

    def get_data_from_db(self, query, db_conn):
        if db_conn != None:
            try:
                result_dataFrame = pd.read_sql(query, db_conn)
                return (result_dataFrame)
            except:
                # printing stack trace
                traceback.print_exc()
                return None
