"""
Created on Wed Mar 15 10:17:00 2023

@author: abhik_bhattacharjee
"""

DB_USERNAME = 'root'
DB_PASSWORD = 'rootpassword'
DB_IP = '35.189.86.249'
DB_PORT = '3306'
DB_CONTROL_DATABASE = 'ase_scm_cntl'

DUBLIN_BIKES_API_URL = "https://data.smartdublin.ie/dublinbikes-api/last_snapshot/"
DART_URL = 'http://api.irishrail.ie/realtime/realtime.asmx/getAllStationsXML_WithStationType?StationType=D'
DART_STN = 'https://api.irishrail.ie/realtime/realtime.asmx/getStationDataByNameXML?StationDesc='

BASE_DATA_BASE = 'dev1_ase_scm'
STAGE_DATA_BASE = 'dev2_ase_scm'
PROD_DATA_BASE = 'dev3_ase_scm'

# Table Names DART
DART_BASE_TABLE = 'bs_irl_dart_stn_rstr'
DART_STAGE_TABLE = 'stg_irl_dart_stn_rstr'
DART_PROD_TABLE = 'dim_irl_dart_stn_rstr'
DART_STATION_SCHEDULE_BASE_TABLE = 'bs_irl_dart_stn_schd'
DART_STATION_SCHEDULE_STAGE_TABLE = 'stg_irl_dart_stn_schd'
DART_STATION_SCHEDULE_PROD_TABLE = 'dim_irl_dart_stn_schd'

# Table Names FootFall
FOOTFALL_BASE_TABLE = 'bs_irl_dub_ppl_ftfl'
FOOTFALL_STAGE_TABLE = 'stg_irl_dub_ppl_ftfl'
FOOTFALL_PROD_TABLE_1 = 'dim_irl_dub_ppl_ftfl'
FOOTFALL_PROD_TABLE_2 = 'dim2_irl_dub_ppl_ftfl'

# Table Name dublin Bike
DUBLIN_BIKE_BASE_TABLE = 'bs_irl_dub_bks'
DUBLIN_BIKE_STAGE_TABLE = 'stg_irl_dub_bks'
DUBLIN_BIKE_PROD_TABLE = 'dim_irl_dub_bks'

# Air Sensor details
AIR_SENSOR_USERNAME = "dublincityapi"
AIR_SENESOR_PASSWORD = "Xpa5vAQ9ki"
AIR_SENSOR_LIST_URL = "https://data.smartdublin.ie/sonitus-api/api/monitors"
AIR_SENSOR_SENSOR_DATA_PULL = "https://data.smartdublin.ie/sonitus-api/api/data"

FOOTFALL_COLS = ["Time", "Aston Quay/Fitzgeralds",
                 "Bachelors walk/Bachelors way",
                 "Baggot st lower/Wilton tce inbound",
                 "Baggot st upper/Mespil rd/Bank",
                 "Capel st/Mary street",
                 "College Green/Bank Of Ireland",
                 "College Green/Church Lane",
                 "College st/Westmoreland st",
                 "D'olier st/Burgh Quay",
                 "Dame Street/Londis",
                 "Dawson Street/Molesworth",
                 "Grafton st/Monsoon Pedestrian",
                 "Grafton Street / Nassau Street / Suffolk Street",
                 "Grand Canal st upp/Clanwilliam place",
                 "Henry Street/Coles Lane/Dunnes",
                 "Liffey st/Halfpenny Bridge ",
                 "Mary st/Jervis st",
                 "Newcomen Bridge/Charleville mall inbound",
                 "North Wall Quay/Samuel Beckett bridge East",
                 "O'Connell St/Parnell St/AIB",
                 "O'Connell st/Princes st North",
                 "Phibsborough Rd/Enniskerry Road",
                 "Richmond st south/Portabello Harbour inbound",
                 "Talbot st/Guineys",
                 "Westmoreland Street East/Fleet street",
                 "Westmoreland Street West/Carrolls"]

FOOTFALL_RENAME = {"Aston Quay/Fitzgeralds": "Aston_Quay",
                   "Bachelors walk/Bachelors way": "Bachelors_walk",
                   "Baggot st lower/Wilton tce inbound": "Baggot_st_lower",
                   "Baggot st upper/Mespil rd/Bank": "Baggot_st_upper",
                   "Capel st/Mary street": "Capel_st",
                   "College Green/Bank Of Ireland": "College_Green_Bank_Of_Ireland",
                   "College Green/Church Lane": "College_Green_Church_Lane",
                   "College st/Westmoreland st": "Westmoreland_st",
                   "D'olier st/Burgh Quay": "Burgh_Quay",
                   "Dame Street/Londis": "Dame_Street",
                   "Dawson Street/Molesworth": "Dawson_Street",
                   "Grafton st/Monsoon Pedestrian": "Grafton_st",
                   "Grafton Street / Nassau Street / Suffolk Street": "Nassau_Street",
                   "Grand Canal st upp/Clanwilliam place": "Clanwilliam_place",
                   "Henry Street/Coles Lane/Dunnes": "Henry_Street",
                   "Liffey st/Halfpenny Bridge ": "Halfpenny_Bridge",
                   "Mary st/Jervis st": "Mary_st",
                   "Newcomen Bridge/Charleville mall inbound": "Newcomen_Bridge",
                   "North Wall Quay/Samuel Beckett bridge East": "North_Wall_Quay",
                   "O'Connell St/Parnell St/AIB": "Parnell_St",
                   "O'Connell st/Princes st North": "Princes_st_North",
                   "Phibsborough Rd/Enniskerry Road": "Enniskerry_Road",
                   "Richmond st south/Portabello Harbour inbound": "Richmond_st_south",
                   "Talbot st/Guineys": "Talbot_st",
                   "Westmoreland Street East/Fleet street": "Fleet_street",
                   "Westmoreland Street West/Carrolls": "Westmoreland_Street"}

STN_DESC = ['Malahide', 'Portmarnock', 'Clongriffin', 'Sutton', 'Bayside', 'Howth Junction', 'Howth', 'Kilbarrack',
            'Raheny', 'Harmonstown', 'Killester', 'Clontarf Road', 'Dublin Connolly', 'Tara Street', 'Dublin Pearse',
            'Grand Canal Dock', 'Lansdowne Road', 'Sandymount', 'Sydney Parade', 'Booterstown', 'Blackrock', 'Seapoint',
            'Salthill and Monkstown', 'Greystones', 'Dun Laoghaire']

FOOT_FALL_URL = 'https://data.smartdublin.ie/dataset/dublin-city-centre-footfall-counters'
