import Source.constants.config as config
from Source.core.DartDataPull import DartDataPull
import unittest

class TestDartDataPull(unittest.TestCase):

    def test_api_res_df_dart(self):
        dart_data_pull = DartDataPull()
        result=dart_data_pull.api_res_df_dart(config.DART_URL)
        if result.empty:
            assert False
        else:
            assert True

    def test_data_dart_to_db(self):
        dart_data_pull = DartDataPull()
        result = dart_data_pull.data_dart_to_db()
        if result:
            assert True
        else:
            assert False




