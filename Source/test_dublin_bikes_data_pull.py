import unittest
from Source.core.DublinBikesDataPull import DublinBikesDataPull


class TestDublinBikesDataPull(unittest.TestCase):
    def test_api_res_df(self):
        dublin_bike_data_pull = DublinBikesDataPull()
        result=dublin_bike_data_pull.api_res_df()
        if result.empty:
            assert False
        else:
            assert True

    def test_dublin_bike_data_to_db(self):
        dublin_bike_data_pull = DublinBikesDataPull()
        result=dublin_bike_data_pull.dublin_bike_data_to_db()
        if result:
            assert True
        else:
            assert False



if __name__ == '__main__':
    unittest.main()
