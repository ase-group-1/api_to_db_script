import unittest
from Source.core.DublinAirSensorDetails import DublinAirSensorDetails


class TestDublinAirSensorDetails(unittest.TestCase):
    def test_get_sensor_data(self):
        dublin_air_sensor_details = DublinAirSensorDetails()
        result= dublin_air_sensor_details.get_sensor_data("TNO4488")
        if result :
            assert True
        else:
            assert False

    def test_get_list_of_air_sensors(self):
        dublin_air_sensor_details = DublinAirSensorDetails()
        result=dublin_air_sensor_details.get_list_of_air_sensors()
        if result.empty:
            assert False
        else:
            assert True

    def test_sensor_details_to_db (self):
        dublin_air_sensor_details = DublinAirSensorDetails()
        result = dublin_air_sensor_details.sensor_details_to_db()
        if result:
            assert True
        else:
            assert False




if __name__ == '__main__':
    unittest.main()
