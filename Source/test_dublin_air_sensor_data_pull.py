import unittest
from Source.core.DublinAirSensorDataPull import DublinAirSensorDataPull


class TestDublinAirSensorDataPull(unittest.TestCase):
    def test_get_all_sensor_details(self):
        dublin_air_sensor_data_pull = DublinAirSensorDataPull()
        res=dublin_air_sensor_data_pull.get_all_sensor_details()
        if res.empty:
            assert False
        else:
            assert True

    def test_get_sensor_data(self):
        dublin_air_sensor_data_pull = DublinAirSensorDataPull()
        res = dublin_air_sensor_data_pull.get_sensor_data("TNO4488")
        if not res:
            assert False
        else:
            assert True

    def test_update_senor_data_into_tables(self):
        dublin_air_sensor_data_pull = DublinAirSensorDataPull()
        res = dublin_air_sensor_data_pull.update_senor_data_into_tables()
        if not res:
            assert False
        else:
            assert True





if __name__ == '__main__':
    unittest.main()
