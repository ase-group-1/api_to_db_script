import unittest
from Source.core.DartSchDataPull import DartSchDataPull
import Source.constants.config as config


class TestDartSchPull(unittest.TestCase):
    def test_api_res_dart_stng(self):
        dart_sch_data_pull = DartSchDataPull()
        result = dart_sch_data_pull.api_res_dart_stn("Malahide")
        if result.empty:
            assert False
        else:
            assert True

    def test_bs_load_dart_stn(self):
        dart_sch_data_pull = DartSchDataPull()
        result = dart_sch_data_pull.bs_load_dart_stn()
        if not result:
            assert False
        else:
            assert True

    def test_l2_l3_load(self):
        dart_sch_data_pull = DartSchDataPull()
        result = dart_sch_data_pull.l2_l3_load()
        if not result:
            assert False
        else:
            assert True

    def test_load_dart_sch_data(self):
        dart_sch_data_pull = DartSchDataPull()
        result = dart_sch_data_pull.load_dart_sch_data()
        if not result:
            assert False
        else:
            assert True


if __name__ == '__main__':
    unittest.main()
