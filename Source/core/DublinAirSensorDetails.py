import traceback

import requests
import pandas as pd
from datetime import datetime, timedelta
import time
import Source.constants.config as config
from Source.dbhandler.databaseHandeler import DataBaseHandler
from Source.utilities.ApiHandler import ApiHandler

db = DataBaseHandler()


class DublinAirSensorDetails:

    def get_sensor_data(self, serial_no):
        current_date = datetime.now()
        start_date = current_date - timedelta(days=5)
        end_date = current_date
        start_timestamp = int(time.mktime(start_date.timetuple()))
        end_timestamp = int(time.mktime(end_date.timetuple()))
        end_point = config.AIR_SENSOR_SENSOR_DATA_PULL + f"?username={config.AIR_SENSOR_USERNAME}&password={config.AIR_SENESOR_PASSWORD}&monitor={serial_no}&start={start_timestamp}&end={end_timestamp}"
        keys_names = list()
        try:
            res = ApiHandler.api_post_api_content_json(end_point)
            keys_names = list(res[0].keys())
        except:
            print("Empty Data")
        return keys_names

    def get_list_of_air_sensors(self):
        url = config.AIR_SENSOR_LIST_URL + f"?username={config.AIR_SENSOR_USERNAME}&password={config.AIR_SENESOR_PASSWORD}"
        try:
            data_json = ApiHandler.api_post_api_content_json(url)
            data = pd.DataFrame(data_json)
            if (not data.empty):
                data['IsAir'] = data['label'].apply(lambda x: x.__contains__('Air'))
                data = data[(data['IsAir'] == True) & (data['last_calibrated'] == '0000-00-00')]
                data['Key_Values'] = data['serial_number'].apply(self.get_sensor_data)
                data['Key_Values'] = data['Key_Values'].astype(str)
                data['IsFieldsPresent'] = data['Key_Values'].apply(
                    lambda x: (x.__contains__('datetime') & x.__contains__('pm2_5')))
                data = data[data['IsFieldsPresent'] == True]
                data.reset_index(inplace=True, drop=True)
                return data
        except:
            print("Data Not found ")
            return pd.DataFrame()

    def sensor_details_to_db(self):
        data = self.get_list_of_air_sensors()
        # To store Data in DB
        if not data.empty:
            try:
                cursor, cnx = db.sql_con_db()
                cursor.execute("TRUNCATE TABLE dev1_ase_scm.bs_irl_dub_air_snsr_rstr")

                for index, row in data.iterrows():
                    insert_stmt = "INSERT INTO dev1_ase_scm.bs_irl_dub_air_snsr_rstr (sr_nm, lbl_txt, loc_nm, lat, " \
                                  "longi) values(%s,%s,%s,%s,%s) "
                    data = (row.serial_number, row.label, row.location, row.latitude, row.longitude)
                    cursor.execute(insert_stmt, data)

                cursor.execute(
                    "with curr_cycl as ( select cycl_time_id from ase_scm_cntl.cntl_etl_sql where rqt_nm = "
                    "\"ASE_DUB_SNSR_RSTR\" ) update ase_scm_cntl.cntl_etl_sql set lst_updt_cycl = (select "
                    "cycl_time_id from curr_cycl) where rqt_nm = \"ASE_DUB_SNSR_RSTR\"")
                cursor.execute(
                    "update ase_scm_cntl.cntl_etl_sql set cycl_time_id = concat( '1', year(sysdate()), lpad(month("
                    "sysdate()),2,0), lpad(day(sysdate()),2,0), lpad(hour(sysdate()),2,0), lpad(minute(sysdate()),2,"
                    "0) ) where rqt_nm = \"ASE_DUB_SNSR_RSTR\"")

                cursor.execute(
                    "delete from dev2_ase_scm.stg_irl_dub_air_snsr_rstr where cycl_time_id = ( select cycl_time_id "
                    "from ase_scm_cntl.cntl_etl_sql where rqt_nm = \"ASE_DUB_SNSR_RSTR\" )")
                cursor.execute(
                    "insert into dev2_ase_scm.stg_irl_dub_air_snsr_rstr ( cycl_time_id, sr_nm, lbl_txt, loc_nm, lat, "
                    "longi) select ( select cycl_time_id from ase_scm_cntl.cntl_etl_sql where rqt_nm = "
                    "\"ASE_DUB_SNSR_RSTR\" ), sr_nm, lbl_txt, loc_nm, lat, longi from "
                    "dev1_ase_scm.bs_irl_dub_air_snsr_rstr")

                cursor.execute("truncate dev3_ase_scm.dim_irl_dub_air_snsr_rstr")
                cursor.execute(
                    "insert into dev3_ase_scm.dim_irl_dub_air_snsr_rstr( cycl_time_id, sr_nm,lbl_txt, loc_nm, lat, "
                    "longi ) select cycl_time_id, sr_nm, lbl_txt, loc_nm, lat, longi from "
                    "dev2_ase_scm.stg_irl_dub_air_snsr_rstr where cycl_time_id = ( select cycl_time_id from "
                    "ase_scm_cntl.cntl_etl_sql where rqt_nm = \"ASE_DUB_SNSR_RSTR\" )")
                cnx.commit()
                return True
            except:
                traceback.print_exc()
                print("Something Went Wrong")
                return False


if __name__ == '__main__':
    obj = DublinAirSensorDetails()
    obj.sensor_details_to_db()
    print("Done")
