import traceback

import pandas as pd
import Source.constants.config as config
from Source.utilities.ApiHandler import ApiHandler
from Source.dbhandler.databaseHandeler import DataBaseHandler

db = DataBaseHandler()


class FootfallDataPull:

    def foot_fall_df(self, url):
        try:
            df = pd.read_csv(url)
            if df.empty:
                return pd.DataFrame(), pd.DataFrame()
            df = df[config.FOOTFALL_COLS]
            df_new = df.rename(columns=config.FOOTFALL_RENAME)
            df_new = df_new.fillna(0, axis=1)
            return df, df_new
        except:
            traceback.print_exc()
            return pd.DataFrame(),pd.DataFrame()

    def foot_fall_to_db(self):
        try:
            crsr, cnx = db.sql_con_db()
            if not db.check_table_exists(config.BASE_DATA_BASE, config.FOOTFALL_BASE_TABLE):
                return False
            if not db.check_table_exists(config.STAGE_DATA_BASE, config.FOOTFALL_STAGE_TABLE):
                return False
            if not db.check_table_exists(config.PROD_DATA_BASE, config.FOOTFALL_PROD_TABLE_1):
                return False
            if not db.check_table_exists(config.PROD_DATA_BASE, config.FOOTFALL_PROD_TABLE_2):
                return False

            crsr.execute("TRUNCATE TABLE dev1_ase_scm.bs_irl_dub_ppl_ftfl;")

            url = ApiHandler.fetch_latest_footfall_csv()
            print(url)
            df, df_new = self.foot_fall_df(url)

            for index, row in df_new.iterrows():
                insert_stmt = "INSERT INTO dev1_ase_scm.bs_irl_dub_ppl_ftfl (time, Aston_Quay , Bachelors_walk , " \
                              "Baggot_st_lower , Baggot_st_upper , Capel_st , College_Green_Bank_Of_Ireland , " \
                              "College_Green_Church_Lane , Westmoreland_st , Burgh_Quay , Dame_Street , Dawson_Street " \
                              ", Grafton_st , Nassau_Street , Clanwilliam_place , Henry_Street , Halfpenny_Bridge , " \
                              "Mary_st , Newcomen_Bridge , North_Wall_Quay , Parnell_St , Princes_st_North , " \
                              "Enniskerry_Road , Richmond_st_south , Talbot_st , Fleet_street , Westmoreland_Street) " \
                              "values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s," \
                              "%s) "
                data = (
                    row.Time, row["Aston_Quay"], row["Bachelors_walk"], row["Baggot_st_lower"], row["Baggot_st_upper"],
                    row["Capel_st"], row["College_Green_Bank_Of_Ireland"], row["College_Green_Church_Lane"],
                    row["Westmoreland_st"], row["Burgh_Quay"], row["Dame_Street"], row["Dawson_Street"],
                    row["Grafton_st"],
                    row["Nassau_Street"], row["Clanwilliam_place"], row["Henry_Street"], row["Halfpenny_Bridge"],
                    row["Mary_st"], row["Newcomen_Bridge"], row["North_Wall_Quay"], row["Parnell_St"],
                    row["Princes_st_North"], row["Enniskerry_Road"], row["Richmond_st_south"], row["Talbot_st"],
                    row["Fleet_street"], row["Westmoreland_Street"])
                crsr.execute(insert_stmt, data)

            crsr.execute(
                "with curr_cycl as ( select cycl_time_id from ase_scm_cntl.cntl_etl_sql where rqt_nm = "
                "\"ASE_DUB_FTFL\" ) update ase_scm_cntl.cntl_etl_sql set lst_updt_cycl = (select cycl_time_id from "
                "curr_cycl) where rqt_nm = \"ASE_DUB_FTFL\"")
            crsr.execute(
                "update ase_scm_cntl.cntl_etl_sql set cycl_time_id = concat( '1', year(sysdate()), lpad(month("
                "sysdate()),2,0), lpad(day(sysdate()),2,0), lpad(hour(sysdate()),2,0), lpad(minute(sysdate()),2,"
                "0) ) where rqt_nm = \"ASE_DUB_FTFL\"")

            crsr.execute(
                "delete from dev2_ase_scm.stg_irl_dub_ppl_ftfl where cycl_time_id = ( select cycl_time_id from "
                "ase_scm_cntl.cntl_etl_sql where rqt_nm = \"ASE_DUB_FTFL\" )")
            crsr.execute(
                "insert into dev2_ase_scm.stg_irl_dub_ppl_ftfl (cycl_time_id, time, Aston_Quay , Bachelors_walk , "
                "Baggot_st_lower , Baggot_st_upper , Capel_st , College_Green_Bank_Of_Ireland , "
                "College_Green_Church_Lane , Westmoreland_st , Burgh_Quay , Dame_Street , Dawson_Street , Grafton_st "
                ", Nassau_Street , Clanwilliam_place , Henry_Street , Halfpenny_Bridge , Mary_st , Newcomen_Bridge , "
                "North_Wall_Quay , Parnell_St , Princes_st_North , Enniskerry_Road , Richmond_st_south , Talbot_st , "
                "Fleet_street , Westmoreland_Street) select (select cycl_time_id from ase_scm_cntl.cntl_etl_sql where "
                "rqt_nm = \"ASE_DUB_FTFL\") as cycl_time_id, str_to_date(time, '%d/%m/%Y %H:%i'), Aston_Quay , "
                "Bachelors_walk , Baggot_st_lower , Baggot_st_upper , Capel_st , College_Green_Bank_Of_Ireland , "
                "College_Green_Church_Lane , Westmoreland_st , Burgh_Quay , Dame_Street , Dawson_Street , Grafton_st "
                ", Nassau_Street , Clanwilliam_place , Henry_Street , Halfpenny_Bridge , Mary_st , Newcomen_Bridge , "
                "North_Wall_Quay , Parnell_St , Princes_st_North , Enniskerry_Road , Richmond_st_south , Talbot_st , "
                "Fleet_street , Westmoreland_Street from dev1_ase_scm.bs_irl_dub_ppl_ftfl")

            crsr.execute("truncate dev3_ase_scm.dim_irl_dub_ppl_ftfl")
            crsr.execute(
                "insert into dev3_ase_scm.dim_irl_dub_ppl_ftfl (cycl_time_id, time, Aston_Quay , Bachelors_walk , "
                "Baggot_st_lower , Baggot_st_upper , Capel_st , College_Green_Bank_Of_Ireland , "
                "College_Green_Church_Lane , Westmoreland_st , Burgh_Quay , Dame_Street , Dawson_Street , Grafton_st "
                ", Nassau_Street , Clanwilliam_place , Henry_Street , Halfpenny_Bridge , Mary_st , Newcomen_Bridge , "
                "North_Wall_Quay , Parnell_St , Princes_st_North , Enniskerry_Road , Richmond_st_south , Talbot_st , "
                "Fleet_street) select (select cycl_time_id from ase_scm_cntl.cntl_etl_sql where rqt_nm = "
                "\"ASE_DUB_FTFL\") as cycl_time_id, time, Aston_Quay , Bachelors_walk , Baggot_st_lower , "
                "Baggot_st_upper , Capel_st , College_Green_Bank_Of_Ireland , College_Green_Church_Lane , "
                "Westmoreland_st , Burgh_Quay , Dame_Street , Dawson_Street , Grafton_st , Nassau_Street , "
                "Clanwilliam_place , Henry_Street , Halfpenny_Bridge , Mary_st , Newcomen_Bridge , North_Wall_Quay , "
                "Parnell_St , Princes_st_North , Enniskerry_Road , Richmond_st_south , Talbot_st , Fleet_street from "
                "dev2_ase_scm.stg_irl_dub_ppl_ftfl where cycl_time_id = (select cycl_time_id from "
                "ase_scm_cntl.cntl_etl_sql where rqt_nm = \"ASE_DUB_FTFL\")")

            crsr.execute("truncate dev3_ase_scm.dim2_irl_dub_ppl_ftfl")
            crsr.execute(
                "insert into dev3_ase_scm.dim2_irl_dub_ppl_ftfl select * from dev3_ase_scm.dim_irl_dub_ppl_ftfl where "
                "convert(time, date) = (select max(convert(time,date)) from dev3_ase_scm.dim_irl_dub_ppl_ftfl)")

            cnx.commit()
            crsr.close()
            return True
        except:
            traceback.print_exc()
            return False


if __name__ == '__main__':
    dbOps = FootfallDataPull()
    dbOps.foot_fall_to_db()
