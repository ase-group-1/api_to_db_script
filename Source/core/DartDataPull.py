import traceback
import Source.constants.config as config
from Source.dbhandler.databaseHandeler import DataBaseHandler
from Source.utilities.ApiHandler import ApiHandler
import pandas as pd

db = DataBaseHandler()


class DartDataPull:

    def api_res_df_dart(self, url):
        """
        To get dart data from url
        :param url: endpoint for dart
        :return: data frame
        """
        try:
            api_res = ApiHandler.api_get_from_xml_dict(url)
            data_df = pd.DataFrame(api_res['ArrayOfObjStation']['objStation'])
            return data_df
        except :
            print("Could Not Convert API Response to DF! SYSTEM FAILED!")
            return pd.DataFrame()

    def data_dart_to_db(self):
        try:
            crsr, cnx = db.sql_con_db()

            dart_base_exists = db.check_table_exists(config.BASE_DATA_BASE, config.DART_BASE_TABLE)
            dart_stage_exists = db.check_table_exists(config.STAGE_DATA_BASE, config.STAGE_DATA_BASE)
            dart_prod_exists = db.check_table_exists(config.PROD_DATA_BASE, config.DART_PROD_TABLE)

            if not dart_base_exists and not dart_stage_exists and not dart_prod_exists:
                print("Disturbance in tables exists")
                return False

            data_df = self.api_res_df_dart(config.DART_URL)

            crsr.execute("TRUNCATE TABLE " + config.BASE_DATA_BASE + "." + config.DART_BASE_TABLE + ";")

            for index, row in data_df.iterrows():
                insert_stmt = "INSERT INTO dev1_ase_scm.bs_irl_dart_stn_rstr (stn_desc, lat, longi, stn_cd, stn_id) " \
                              "values(%s,%s,%s,%s,%s) "
                data = (row.StationDesc, row.StationLatitude, row.StationLongitude, row.StationCode, row.StationId)
                crsr.execute(insert_stmt, data)

            crsr.execute(
                "with curr_cycl as ( select cycl_time_id from ase_scm_cntl.cntl_etl_sql where rqt_nm = "
                "\"ASE_DART_RSTR\" ) update ase_scm_cntl.cntl_etl_sql set lst_updt_cycl = (select cycl_time_id from "
                "curr_cycl) where rqt_nm = \"ASE_DART_RSTR\"")
            crsr.execute(
                "update ase_scm_cntl.cntl_etl_sql set cycl_time_id = concat( '1', year(sysdate()), lpad(month("
                "sysdate()),2,0), lpad(day(sysdate()),2,0), lpad(hour(sysdate()),2,0), lpad(minute(sysdate()),2,"
                "0) ) where rqt_nm = \"ASE_DART_RSTR\"")

            crsr.execute(
                "delete from dev2_ase_scm.stg_irl_dart_stn_rstr where cycl_time_id = ( select cycl_time_id from "
                "ase_scm_cntl.cntl_etl_sql where rqt_nm = \"ASE_DART_RSTR\" );")
            crsr.execute(
                "insert into dev2_ase_scm.stg_irl_dart_stn_rstr (cycl_time_id, stn_desc, lat, longi, stn_cd, "
                "stn_id)  select (select cycl_time_id from ase_scm_cntl.cntl_etl_sql where rqt_nm = "
                "\"ASE_DART_RSTR\"), stn_desc, lat, longi, stn_cd, cast(stn_id as UNSIGNED) as stn_id from "
                "dev1_ase_scm.bs_irl_dart_stn_rstr")

            crsr.execute("truncate dev3_ase_scm.dim_irl_dart_stn_rstr")
            crsr.execute(
                "insert into dev3_ase_scm.dim_irl_dart_stn_rstr (cycl_time_id, stn_desc, lat, longi, stn_cd, "
                "stn_id) select cycl_time_id, stn_desc, lat, longi, stn_cd, stn_id from "
                "dev2_ase_scm.stg_irl_dart_stn_rstr where cycl_time_id = (select cycl_time_id from "
                "ase_scm_cntl.cntl_etl_sql where rqt_nm = \"ASE_DART_RSTR\")")

            cnx.commit()
            crsr.close()
            return True
        except:
            traceback.print_exc()
            return False


if __name__ == '__main__':
    dbOps = DartDataPull()
    dbOps.data_dart_to_db()
