import traceback

import pandas as pd

import Source.constants.config as config
from Source.dbhandler.databaseHandeler import DataBaseHandler
from Source.utilities.ApiHandler import ApiHandler

import warnings

warnings.filterwarnings("ignore")
db = DataBaseHandler()


class DublinBikesDataPull:

    def api_res_df(self):
        try:
            data_df = ApiHandler.api_get_df_from_api(config.DUBLIN_BIKES_API_URL)
            return data_df
        except:
            print("Could Not Convert API Response to DF! SYSTEM FAILED!")
            return pd.DataFrame()

    def dublin_bike_data_to_db(self):
        try:

            crsr, cnx = db.sql_con_db()
            if not db.check_table_exists(config.BASE_DATA_BASE, config.DUBLIN_BIKE_BASE_TABLE):
                return False
            if not db.check_table_exists(config.STAGE_DATA_BASE, config.DUBLIN_BIKE_STAGE_TABLE):
                return False
            if not db.check_table_exists(config.PROD_DATA_BASE, config.DUBLIN_BIKE_PROD_TABLE):
                return False

            crsr.execute("TRUNCATE TABLE dev1_ase_scm.bs_irl_dub_bks;")
            data = self.api_res_df()

            for index, row in data.iterrows():
                insert_stmt = "INSERT INTO dev1_ase_scm.bs_irl_dub_bks (id, hrvst_ts, stn_id, avl_bks_std, bks_std, avl_bks, bnk, bns, ls_updt_ts, sta, addr_ln, stn_nm, lat, longi) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                data = (row.id, row.harvest_time, row.station_id, row.available_bike_stands, row.bike_stands,
                        row.available_bikes, row.banking, row.bonus, row.last_update, row.status, row.address, row.name,
                        row.latitude, row.longitude)
                crsr.execute(insert_stmt, data)

            crsr.execute(
                "with curr_cycl as ( select cycl_time_id from ase_scm_cntl.cntl_etl_sql where rqt_nm = \"ASE_DUB_BKS\" ) update ase_scm_cntl.cntl_etl_sql set lst_updt_cycl = (select cycl_time_id from curr_cycl) where rqt_nm = \"ASE_DUB_BKS\"")
            crsr.execute(
                "update ase_scm_cntl.cntl_etl_sql set cycl_time_id = concat( '1', year(sysdate()), lpad(month(sysdate()),2,0), lpad(day(sysdate()),2,0), lpad(hour(sysdate()),2,0), lpad(minute(sysdate()),2,0) ) where rqt_nm = \"ASE_DUB_BKS\"")

            crsr.execute(
                "delete from dev2_ase_scm.stg_irl_dub_bks where cycl_time_id = ( select cycl_time_id from ase_scm_cntl.cntl_etl_sql where rqt_nm = \"ASE_DUB_BKS\" )")
            crsr.execute(
                "insert into dev2_ase_scm.stg_irl_dub_bks ( cycl_time_id, id, hrvst_ts, stn_id, avl_bks_std, bks_std, avl_bks, bnk, bns, ls_updt_ts, sta, addr_ln, stn_nm, lat, longi ) select ( select cycl_time_id from ase_scm_cntl.cntl_etl_sql where rqt_nm = \"ASE_DUB_BKS\" ), cast(id as UNSIGNED) as id, convert(hrvst_ts, DATETIME) as hrvst_ts, cast(stn_id as UNSIGNED) as stn_id, cast(avl_bks_std as UNSIGNED) as avl_bks_std, cast(bks_std as UNSIGNED) as bks_std, cast(avl_bks as UNSIGNED) as avl_bks, cast(bnk as UNSIGNED) as bnk, cast(bns as UNSIGNED) as bns, convert(ls_updt_ts, DATETIME) as ls_updt_ts, sta, addr_ln, stn_nm, lat, longi from dev1_ase_scm.bs_irl_dub_bks")

            crsr.execute("truncate dev3_ase_scm.dim_irl_dub_bks")
            crsr.execute(
                "insert into dev3_ase_scm.dim_irl_dub_bks( cycl_time_id, id, hrvst_ts, stn_id, avl_bks_std, bks_std, avl_bks, bnk, bns, ls_updt_ts, sta, addr_ln, stn_nm, lat, longi ) select cycl_time_id, id, hrvst_ts, stn_id, avl_bks_std, bks_std, avl_bks, bnk, bns, ls_updt_ts, sta, addr_ln, stn_nm, lat, longi from dev2_ase_scm.stg_irl_dub_bks where cycl_time_id = ( select cycl_time_id from ase_scm_cntl.cntl_etl_sql where rqt_nm = \"ASE_DUB_BKS\" )")

            cnx.commit()
            crsr.close()
            return True
        except:
            traceback.print_exc()
            return False


if __name__ == '__main__':
    dbOps = DublinBikesDataPull()
    dbOps.dublin_bike_data_to_db()
