import traceback
import pandas as pd
import Source.constants.config as config
from Source.dbhandler.databaseHandeler import DataBaseHandler
from Source.utilities.ApiHandler import ApiHandler

db = DataBaseHandler()


class DartSchDataPull:

    def api_res_dart_stn(self, stn):
        try:
            url = config.DART_STN + stn
            api_res_dart_stn = ApiHandler.api_get_from_xml_dict(url)
            if 'ArrayOfObjStationData' in api_res_dart_stn.keys() and 'objStationData' in api_res_dart_stn['ArrayOfObjStationData'].keys ():
                data_dart_stn = pd.DataFrame([api_res_dart_stn['ArrayOfObjStationData']['objStationData']])
            else:
                data_dart_stn= pd.DataFrame()
            return data_dart_stn
        except:
            traceback.print_exc()
            return pd.DataFrame

    def bs_load_dart_stn(self):
        try:
            crsr, cnx = db.sql_con_db()
            crsr.execute("TRUNCATE TABLE " + config.BASE_DATA_BASE + "." + config.DART_STATION_SCHEDULE_BASE_TABLE + " ;")

            for stn in config.STN_DESC:
                dart_stn_df = self.api_res_dart_stn(stn)
                for index, row in dart_stn_df.iterrows():
                    insert_stmt = "INSERT INTO dev1_ase_scm.bs_irl_dart_stn_schd (stn_desc, srvr_ts, trn_cd, stn_nm, stn_cd, qry_ts, trn_dt, org_nm, destn, org_ts, destn_ts, sta, lst_loc, due_ts, late, exp_arr, exp_dep, sch_arr, sch_dep, drtn, trn_typ, loc_typ) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                    data = (
                    stn, row.Servertime, row.Traincode, row.Stationfullname, row.Stationcode, row.Querytime,
                    row.Traindate,
                    row.Origin, row.Destination, row.Origintime, row.Destinationtime, row.Status, row.Lastlocation,
                    row.Duein, row.Late, row.Exparrival, row.Expdepart, row.Scharrival, row.Schdepart, row.Direction,
                    row.Traintype, row.Locationtype)
                    crsr.execute(insert_stmt, data)
            cnx.commit()
            crsr.close()
            return True
        except:
            traceback.print_exc()
            return False

    def l2_l3_load(self):
        try:

            crsr, cnx = db.sql_con_db()
            if not db.check_table_exists(config.STAGE_DATA_BASE, config.DART_STATION_SCHEDULE_STAGE_TABLE):
                print("TABLE NOT FOUND =", config.DART_STATION_SCHEDULE_STAGE_TABLE)
                return False
            if not db.check_table_exists(config.PROD_DATA_BASE, config.DART_STATION_SCHEDULE_PROD_TABLE):
                print("TABLE NOT FOUND =", config.DART_STATION_SCHEDULE_PROD_TABLE)
                return False

            crsr.execute(
                "with curr_cycl as ( select cycl_time_id from ase_scm_cntl.cntl_etl_sql where rqt_nm = "
                "\"ASE_DART_SCHD\" ) update ase_scm_cntl.cntl_etl_sql set lst_updt_cycl = (select cycl_time_id from "
                "curr_cycl) where rqt_nm = \"ASE_DART_SCHD\"")
            crsr.execute(
                "update ase_scm_cntl.cntl_etl_sql set cycl_time_id = concat( '1', year(sysdate()), lpad(month("
                "sysdate()),2,0), lpad(day(sysdate()),2,0), lpad(hour(sysdate()),2,0), lpad(minute(sysdate()),2,"
                "0) ) where rqt_nm = \"ASE_DART_SCHD\"")

            crsr.execute(
                "delete from dev2_ase_scm.stg_irl_dart_stn_schd where cycl_time_id = ( select cycl_time_id from "
                "ase_scm_cntl.cntl_etl_sql where rqt_nm = \"ASE_DART_SCHD\" )")
            crsr.execute(
                "insert into dev2_ase_scm.stg_irl_dart_stn_schd (cycl_time_id, stn_desc, srvr_ts, trn_cd, stn_nm, "
                "stn_cd, qry_ts, trn_dt, org_nm, destn, org_ts, destn_ts, sta, lst_loc, due_ts, late, exp_arr, "
                "exp_dep, sch_arr, sch_dep, drtn, trn_typ, loc_typ) select (select cycl_time_id from "
                "ase_scm_cntl.cntl_etl_sql where rqt_nm = \"ASE_DART_SCHD\") as cycl_time_id, stn_desc, cast(srvr_ts "
                "as datetime) as srvr_ts, trn_cd, stn_nm, stn_cd, cast(qry_ts as TIME) as qry_ts, trn_dt, org_nm, "
                "destn,  cast(org_ts as TIME) as org_ts, cast(destn_ts as TIME) as destn_ts, sta, lst_loc,  "
                "cast(due_ts as UNSIGNED) as due_ts, late, cast(exp_arr as TIME) as exp_arr, cast(exp_dep as TIME) as "
                "exp_dep, cast(sch_arr as TIME) as sch_arr, cast(sch_dep as TIME) as sch_dep, drtn, trn_typ, "
                "loc_typ from dev1_ase_scm.bs_irl_dart_stn_schd")

            crsr.execute("truncate dev3_ase_scm.dim_irl_dart_stn_schd")
            crsr.execute(
                "insert into dev3_ase_scm.dim_irl_dart_stn_schd (cycl_time_id, stn_desc, srvr_ts, trn_cd, stn_nm, "
                "stn_cd, qry_ts, trn_dt, org_nm, destn, org_ts, destn_ts, sta, lst_loc, due_ts, late, exp_arr, "
                "exp_dep, sch_arr, sch_dep, drtn, trn_typ, loc_typ) select cycl_time_id, stn_desc, srvr_ts, trn_cd, "
                "stn_nm, stn_cd, qry_ts, trn_dt, org_nm, destn, org_ts, destn_ts, sta, lst_loc, due_ts, late, "
                "exp_arr, exp_dep, sch_arr, sch_dep, drtn, trn_typ, loc_typ from dev2_ase_scm.stg_irl_dart_stn_schd "
                "where cycl_time_id = ( select cycl_time_id from ase_scm_cntl.cntl_etl_sql where rqt_nm = "
                "\"ASE_DART_SCHD\" )")

            cnx.commit()
            crsr.close()
            return True
        except:
            traceback.print_exc()
            return False
    def load_dart_sch_data(self):
        if self.bs_load_dart_stn():
            return self.l2_l3_load()
        else:
            return False


if __name__ == '__main__':
    dbOps = DartSchDataPull()
    dbOps.load_dart_sch_data()