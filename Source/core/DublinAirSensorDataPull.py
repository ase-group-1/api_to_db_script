import traceback
import requests
import pandas as pd
from datetime import datetime, timedelta
import time
import mysql.connector
import Source.constants.config as config
from Source.dbhandler.databaseHandeler import DataBaseHandler
from Source.utilities.ApiHandler import ApiHandler

db = DataBaseHandler()


class DublinAirSensorDataPull():

    def get_all_sensor_details(self):
        try:
            sql_connection, cursor = db.sql_con_db()
            query = "Select sr_nm as serialNo from dev3_ase_scm.dim_irl_dub_air_snsr_rstr"
            result_data_frame = db.get_data_from_db(query, cursor)
            sql_connection.close()
            return result_data_frame
        except:
            traceback.print_exc()
            return pd.DataFrame()

    def get_sensor_data(self, serial_no):
        current_date = datetime.now()
        start_date = current_date - timedelta(minutes=15)
        end_date = current_date
        start_timestamp = int(time.mktime(start_date.timetuple()))
        end_timestamp = int(time.mktime(end_date.timetuple()))
        end_point = config.AIR_SENSOR_SENSOR_DATA_PULL + f"?username={config.AIR_SENSOR_USERNAME}&password={config.AIR_SENESOR_PASSWORD}&monitor={serial_no}&start={start_timestamp}&end={end_timestamp}"
        try:
            response = ApiHandler.api_post_api_content_json(end_point)
            return response

        except:
            print("No Data Found For  sensor ", serial_no)
            # printing stack trace
            traceback.print_exc()
            return dict()

    def update_senor_data_into_tables(self):
        # Make Sql Connection
        try:

            cursor, cnx = db.sql_con_db()
            sensor_data = self.get_all_sensor_details()
            sensor_data['Response'] = sensor_data['serialNo'].apply(self.get_sensor_data)
            for index, row in sensor_data.iterrows():
                senor_name = row['serialNo']
                response = row['Response']
                if response:
                    # bs_irl_dub_air_snsr_dcc_aq2
                    sensor_lower_name = str(senor_name).strip().lower().replace("-", "_")
                    rqt_name_in_crt_table = "ASE_DUB_SNSR_" + str(senor_name).strip().upper().replace("-", "_")
                    base_table_name = "bs_irl_dub_air_snsr_" + sensor_lower_name
                    stage_table_name = "stg_irl_dub_air_snsr_" + sensor_lower_name
                    prod_table_name = "dim_irl_dub_air_snsr_" + sensor_lower_name
                    is_prod_table_present = db.check_table_exists(config.PROD_DATA_BASE, prod_table_name)
                    is_stage_table_present = db.check_table_exists(config.STAGE_DATA_BASE, stage_table_name)
                    is_base_table_present = db.check_table_exists(config.BASE_DATA_BASE, base_table_name)
                    response_dataframe = pd.DataFrame(response)
                    if not response_dataframe.empty:
                        if is_prod_table_present and is_stage_table_present and is_base_table_present:
                            try:

                                cursor.execute("TRUNCATE TABLE dev1_ase_scm." + str(base_table_name))
                                for index, row in response_dataframe.iterrows():
                                    insert_stmt = "INSERT INTO dev1_ase_scm." + base_table_name + "(datetime, pm2_5) values(%s,%s)"
                                    data = (row.datetime, row.pm2_5)
                                    cursor.execute(insert_stmt, data)
                                    cursor.execute(
                                        "with curr_cycl as ( select cycl_time_id from ase_scm_cntl.cntl_etl_sql where rqt_nm = \"" + rqt_name_in_crt_table + "\" ) update ase_scm_cntl.cntl_etl_sql set lst_updt_cycl = (select cycl_time_id from curr_cycl) where rqt_nm = \"" + rqt_name_in_crt_table + "\"")
                                    cursor.execute(
                                        "update ase_scm_cntl.cntl_etl_sql set cycl_time_id = concat( '1', year(sysdate()), lpad(month(sysdate()),2,0), lpad(day(sysdate()),2,0), lpad(hour(sysdate()),2,0), lpad(minute(sysdate()),2,0) ) where rqt_nm = \"" + rqt_name_in_crt_table + "\"")
                                    cursor.execute(
                                        "delete from dev2_ase_scm." + stage_table_name + " where cycl_time_id = ( select cycl_time_id from ase_scm_cntl.cntl_etl_sql where rqt_nm = \"" + rqt_name_in_crt_table + "\" )")
                                    cursor.execute(
                                        "insert into dev2_ase_scm." + stage_table_name + " (cycl_time_id,datetime, pm2_5) select ( select cycl_time_id from ase_scm_cntl.cntl_etl_sql where rqt_nm = \"" + rqt_name_in_crt_table + "\" ), cast(datetime as datetime) as datetime,cast(pm2_5 as double) as pm2_5  from dev1_ase_scm." + str(
                                            base_table_name))

                                    cursor.execute("truncate dev3_ase_scm." + prod_table_name)
                                    cursor.execute(
                                        "insert into dev3_ase_scm." + prod_table_name + " ( cycl_time_id, datetime,pm2_5) select cycl_time_id, datetime, pm2_5 from dev2_ase_scm." + stage_table_name + " where cycl_time_id = ( select cycl_time_id from ase_scm_cntl.cntl_etl_sql where rqt_nm =\"" + rqt_name_in_crt_table + "\" )")
                            except:
                                print("Excpection Occured")
                                # printing stack trace
                                traceback.print_exc()
                        else:
                            print("Data Base Missing")
                            if not is_base_table_present:
                                print("Base Table Not found :", base_table_name)

                            if not is_prod_table_present:
                                print("Prod Table Not found :", prod_table_name)

                            if not is_stage_table_present:
                                print("Stage Table Not Found :", prod_table_name)

            if not sensor_data.empty:
                cnx.commit()
            cursor.close()
            return True
        except:
            print("Excpection Occured")
            # printing stack trace
            traceback.print_exc()
            return False


if __name__ == '__main__':
    obj = DublinAirSensorDataPull()
    obj.update_senor_data_into_tables()
    print("Done")
